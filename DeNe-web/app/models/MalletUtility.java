
package models;

import cc.mallet.classify.Classifier;
import cc.mallet.classify.ClassifierTrainer;
import cc.mallet.classify.NaiveBayesTrainer;
import cc.mallet.classify.Trial;
import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.FeatureSequence2FeatureVector;
import cc.mallet.pipe.Input2CharSequence;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.PrintInputAndTarget;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.Target2Label;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceLowercase;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import org.jongo.*;
import com.mongodb.*;
import org.bson.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import org.apache.commons.lang.SystemUtils;

/**
 * @author Teban
 */
public class MalletUtility {
    
    String directory = "";    

    public MalletUtility(){
        if(SystemUtils.IS_OS_WINDOWS){
            directory = "c:/DeNe/";
        }
        else{
            directory = "/opt/DeNe";
        }
    }
    
    /**
     * Lee el archivo en el sistema.
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public ArrayList<InstanceUtil> readFile() throws FileNotFoundException, IOException{
        ArrayList<InstanceUtil> instances = new ArrayList();
        BufferedReader br = new BufferedReader(new FileReader("data.csv"));
        try {
            String line = br.readLine();

            while (line != null) {
                String[] lineSplitted = line.split(";");
                if(lineSplitted.length == 3){
                    InstanceUtil newInstance = new InstanceUtil(lineSplitted[0], lineSplitted[1], lineSplitted[2]);
                    instances.add(newInstance);
                }
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        /*for(Instance i: instances){
            System.out.println(i.name +" - "+i.label+" - "+i.content);
        }*/
        return instances;
    }
    
    /**
     * recibe el archivo como un string donde cada linea se separa por el caracter \n
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public String readFileRaw() throws FileNotFoundException, IOException{
        String output = "";
        BufferedReader br = new BufferedReader(new FileReader("data.csv"));
        try {
            String line = br.readLine();
            
            while (line != null) {
                output+=line+"\n";
                line = br.readLine();
            }
        } finally {
            br.close();
        }

        return output;
    }
    
    /**
     * transforma el archivo como string a una lista
     * @param file
     * @return 
     */
    public ArrayList<InstanceUtil> receiveFile(String file){
        String[] input = file.split("\n");
        ArrayList<InstanceUtil> output = new ArrayList();
        
        for(String s: input){
            String[] currentLine = s.split(";");
            if(currentLine.length == 3){
                output.add(new InstanceUtil(currentLine[0],currentLine[1],currentLine[2]));
            }
        }
        return output;
    }
    
    /**
     * aplica los filtros del TextNormalizer
     * @param input
     * @return 
     */
    public ArrayList<InstanceUtil> cleanData(ArrayList<InstanceUtil> input){
        TextNormalizer tN = new TextNormalizer();
        for(InstanceUtil i: input){
            i.content = tN.normalizeText(123, i.content);
        }
        return input;
    }
    
    /**
     * crea una lista de instancias pasando las instancias de InstanceUtil dadas en un arraylist
     * @param inst
     * @return 
     */
    public InstanceList prepareInstance(ArrayList<InstanceUtil> inst){
        Pipe pipe = buildPipe();
        InstanceList iL = new InstanceList(pipe);
        for(InstanceUtil iU : inst){
            iL.addThruPipe(new Instance(iU.content, iU.label, iU.name, "NULL"));
        }
        return iL;
    }
    
    /**
     * prepara el pipe de limpieza y transformacion de datos.
     * @return 
     */
    public Pipe buildPipe(){//ArrayList<Instance> input){
        ArrayList pipeList = new ArrayList();
        pipeList.add(new Input2CharSequence("UTF-8"));
        pipeList.add(new CharSequence2TokenSequence());
        pipeList.add(new TokenSequenceLowercase());
        pipeList.add(new TokenSequence2FeatureSequence());
        pipeList.add(new Target2Label());
        pipeList.add(new FeatureSequence2FeatureVector());
        pipeList.add(new PrintInputAndTarget());
        return new SerialPipes(pipeList);
    }
    
    /**
     * entrena un clasificador naive bayes.
     * @param iL
     * @return 
     */
    public Classifier createNBClassifier(InstanceList iL){
        ClassifierTrainer cT = new NaiveBayesTrainer();
        return cT.train(iL);
    }
    
    /**
     * Persiste el clasificador en un archivo .dene
     * @param classifier
     * @throws IOException 
     */
    public void saveClassifier(Classifier classifier) throws IOException{
        try{
            removePreviousClassifier();
        }
        catch(Exception e){
            //foo
        }
        ObjectOutputStream oos =
            new ObjectOutputStream(new FileOutputStream (new File(directory+"classifier.dene")));
        oos.writeObject (classifier);
        oos.close();
    }

    
    /**
     * Lee un clasificador previamente guardado.
     * @return
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public Classifier readClassifier() throws IOException, ClassNotFoundException{
        Classifier classifier = null;

        try{
            ObjectInputStream ois =
            new ObjectInputStream (new FileInputStream (directory+"classifier.dene"));
            classifier = (Classifier) ois.readObject();
            ois.close();

            
        }
        catch(IOException e){

        }
        return classifier;
    }
    /**
     * Elimina el clasificador actual.
     */
    public void removePreviousClassifier() throws FileNotFoundException{
        File file = new File(directory+"classifier.dene");
        if(file.canRead()){
           while(file.delete()){
                System.out.println("El archivo está en uso, reintentando.");
           }
           System.out.println("Clasificador antiguo eliminado."); 
        }
        else{
            System.out.println("Archivo no encontrado o no legible.");
        }
    }
    
    /**
     * Evalua el clasificador.
     * @param classifier
     * @param iU 
     */
    public double evaluate(Classifier classifier, ArrayList<InstanceUtil> iU){
        try{
        InstanceList testInstances = new InstanceList(classifier.getInstancePipe());
        for(InstanceUtil i : iU){
            testInstances.addThruPipe(new Instance(i.content, i.label, i.name, "NULL"));
        }
        Trial trial = new Trial(classifier, testInstances);
        
        //System.out.println("Accuracy: " + trial.getAccuracy());
        
        return trial.getAccuracy();
        }
        catch(Exception e){
            return 0;
        }
    }
    
    /**
     * compara dos clasificadores y mantiene el mejor.
     * @param actual
     * @param nuevo
     * @param iU
     * @return
     * @throws IOException 
     */
    public ArrayList<String> judgeClassifier(Classifier actual, Classifier nuevo, ArrayList<InstanceUtil> iU) throws IOException{
        double actualC = this.evaluate(actual, iU);
        double nuevoC = this.evaluate(nuevo, iU);
        ArrayList<String> output = new ArrayList();
        if(actualC >= nuevoC){
            output.add("old");
            output.add(""+actualC);
            output.add(""+nuevoC);
            return output;
        }
        else{
            output.add("new");
            output.add(""+actualC);
            output.add(""+nuevoC);
            return output;
        }
    }
}
