package models;

import java.io.Serializable;
import java.util.Date;
import org.bson.types.ObjectId;
import twitter4j.Status;

/**
 *
 * @author teban
 */
public class StatusMongo implements Serializable{

    public ObjectId _id;
    public Status tweet;
    public Date timestamp;
}
