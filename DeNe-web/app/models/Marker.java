package models;

import org.bson.types.ObjectId;
import java.util.Date;

/**
* Clase de marcadores para el mapeo de objetos JSON
*/
public class Marker{

	public ObjectId _id;
	public String contenido;
	public String categoria;
    public Double latitud;
    public Double longitud;
    public String userID;	 
    public Date generatedAt;
    public Marker(){}
}