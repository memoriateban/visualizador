package models;

import org.bson.types.ObjectId;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

/**
* Clase de marcadores para el mapeo de objetos JSON
*/
public class Query{

	public ObjectId _id;
	public ArrayList<String> terminos;
	public String estado; 
    public Date generatedAt;
    //public int tweetCount;
    public Query(){
    	terminos = new ArrayList();
    }
    public void cleanUp(){
    	terminos.clear();
    }
}