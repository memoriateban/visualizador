package controllers;

import play.*;
import play.mvc.*;
import play.api.*;
import views.html.*;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.libs.ws.*;
import javax.inject.Inject;
import java.util.List;
import java.util.ArrayList;
import models.*;
import org.jongo.*;
import com.mongodb.*;
import org.bson.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.Random;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import cc.mallet.classify.Classifier;
import cc.mallet.types.InstanceList;


public class HomeController extends Controller {
    
    String dataBaseName = "DeNe-test";

    public String modelOutput = "";

    /**
    * Variables que el sistema utiliza para verificar si una consulta es o no escrita en la base de datos.
    */
    public Boolean puedeEjecutar = true;

    /**
    * Tiempo (en segundos), dentro de los cuales será válido un tweet.
    */
    public long timeToLive = 3600;

    /**
    * Tiempo (en segundos), dentro de los cuales se actualizará el set de marcadores.
    */
    public int refreshMarkers = 5;


    /**
    * Query actual del sistema.
    */
    public Query q = new Query();

    /**
    * Renderiza la página principal.
    * @return 200 y muestra la pantalla principal
    */

    public Result index(){
        play.Logger.info("Estado del sistema - Puede ejecutar: "+puedeEjecutar);
        play.Logger.info("Estado del sistema - TTL: "+timeToLive);   
        cleanQueries();   
    	return ok(index2.render());
    }

    /**
    * Entrega la consulta que ha de ser seguida para enviar los datos a la base de datos.
    * @return 200 Si el sistema está libre y se guardó la consulta. En caso contrario retorna el error.
    */
    public Result getQuery(){

        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection queries = jongo.getCollection("queries");
        DynamicForm requestData = Form.form().bindFromRequest();
        if (requestData.get("go")!=null) {

            /*BOTÓN DE BÚSQUEDA*/
            if(puedeEjecutar == false){
                //return badRequest(badRequest.render("Ya se está ejecutando una instancia. Pulse Cancelar para iniciar una nueva."));
                q.estado = "antigua";
                queries.save(q);
                q = new Query();
            }
            //ELIMINAR BUSQUEDA ANTERIOR, EMPEZAR NUEVA
            play.Logger.info("Recibido BUSCAR");
            String query = requestData.get("query");
            if(query.equals(""))
                return badRequest(badRequest.render("No se han ingresado consultas. Ingrese sus consultas separadas por coma (\",\")."));
            puedeEjecutar = false;            
            String[] query2 = query.split(",");
            for(String s : query2){
                play.Logger.info("Recibida consulta: "+s+".");
                q.terminos.add(s);
            }
            
            MongoCursor<Query> aQ = queries.find().as(Query.class);
            ArrayList<Query> oldQueries = new ArrayList();
            /*Mantiene SÓLO una query activa, todas las anteriores las setea como "antigua" ...*/
            while(aQ.hasNext()){
                Query nQ = aQ.next();
                if(nQ.estado == "actual")
                    nQ.estado = "antigua";
                    oldQueries.add(nQ);
            }
            for(Query qL : oldQueries){
                queries.save(qL);
            }
            /*... Y guarda la nueva*/
            q.estado = "actual";
            q.generatedAt = new Date();
            queries.save(q);
            

        } else if (requestData.get("cancel")!=null) {
            /*BOTON DE CANCELAR*/

            play.Logger.info("Recibido Señal de Término.");
            puedeEjecutar = true;
            /*
                CAMBIAR ESTADO CONSULTA
            */
            q.estado = "antigua";
            queries.save(q);
            q = new Query();
            play.Logger.info("Búsqueda finalizada.");
        }
        return ok(index2.render()); 
    }
    
    /**
    * Setea el parámetro de TTL responsable de la muestra de marcadores en el mapa.
    * @return 200 y redirige a la página de configuración si no hubo problemas. Error en caso contrario.
    */
    public Result setTTL(){
        DynamicForm requestData = Form.form().bindFromRequest();
        String query = requestData.get("TTL");
        try{
            timeToLive = Long.parseLong(query);
            return redirect("/#configDeNe");
        }
        catch(Exception e){
            return badRequest(badRequest.render("No se ha entregado un valor apropiado para TTL.\nSe espera un entero correspondiente un valor temporal en milisegundos."));
        }
    }

    /**
    * Setea el parámetro de refresco de marcadores.
    * @return 200 y redirige a la página de configuración si no hubo problemas. Error en caso contrario.
    */
    public Result setRefreshTime(){
        DynamicForm requestData = Form.form().bindFromRequest();
        String query = requestData.get("markerRefreshValue");
        try{
            refreshMarkers = Integer.parseInt(query);
            return redirect("/#configDeNe");
        }
        catch(Exception e){
            return badRequest(badRequest.render("No se ha entregado un valor apropiado para el valor de refresco. Se esper aun tiempo en segundos."));
        }
    }

    /**
    * Devuelve el valor del parámetro de sistema.
    * @return página en blanco con el valor de Refresco de los marcadores. Puede ser usado como data para función javascript.
    */
    public Result getRefreshTime(){
        return ok(""+refreshMarkers);
    }

    /**
    * Devuelve el valor del parámetro de sistema.
    * @return página en blanco con el valor de TTL. Puede ser usado como data para función javascript.
    */
    public Result getTTLValue(){
        return ok(""+timeToLive);
    }

    public Result gerCurrentQuery(){

        return ok(""+q.terminos.toString());
    }

    public void cleanQueries(){
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection queries = jongo.getCollection("queries");
        MongoCursor<Query> aQ = queries.find().as(Query.class);
        while(aQ.hasNext()){
            Query q = aQ.next();

            if(q.estado.equals("actual")){
                play.Logger.info("Se ha encontrado una inconsistencia en las query. Corrigiendo...");
                q.estado = "antigua";
            }
            queries.save(q);
        }

    }

    /**
    * Devuelve un JSON correspondiente a los Tweets que cumplen con el criterio TTL 
    * desde la base de datos.
    * @return 200 con un arreglo (json) de elementos a visualizar.
    */
    public Result getLocations(){
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection markers = jongo.getCollection("markers");
        MongoCursor<Marker> aM = markers.find().as(Marker.class);
        ArrayList<Marker> aMQ = new ArrayList();
        Date actualDate = new Date();
        while(aM.hasNext()){
            Marker m = aM.next();
            long difference = dateDifference(m.generatedAt, actualDate);
            //play.Logger.info(""+difference);
            if(difference < timeToLive){
                aMQ.add(m);
            }
        }   
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try{
            String json = ow.writeValueAsString(aMQ);  
            return ok(json);//aMQ;
        }
        catch(Exception e){
            return badRequest(badRequest.render("No se pudo completar la solicitud."));
        }
    }

    /**
    * Devuelve un JSON correspondiente a los Tweets que cumplen con el criterio de las fechas
    * desde la base de datos.
    * @return 200 con un arreglo (json) de elementos a visualizar.
    */
    public Result getLocationsTimeline(String d1, String d2){
        //play.Logger.info("\n" + d1 + "\n" + d2);
        Date newDateInit;
        Date newDateEnd;
        try{
        newDateInit = new Date(Long.parseLong(d1));
        newDateEnd = new Date(Long.parseLong(d2));
        }
        catch(Exception e){
            play.Logger.info(e.getLocalizedMessage());
            return badRequest(badRequest.render("No se pudo completar la solicitud."));
        }
        //play.Logger.info("\n"+newDateInit+"\n"+newDateEnd);
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection markers = jongo.getCollection("markers");
        MongoCursor<Marker> aM = markers.find().as(Marker.class);
        ArrayList<Marker> aMQ = new ArrayList();
        for(Marker m : aM){
            if(m.generatedAt.getTime() > newDateInit.getTime() && m.generatedAt.getTime() < newDateEnd.getTime()){
                aMQ.add(m);
            }
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try{
            String json = ow.writeValueAsString(aMQ);  
            return ok(json);//aMQ;
        }
        catch(Exception e){
            return badRequest(badRequest.render("No se pudo completar la solicitud."));
        }
    }

/**
* Devuelve la diferencia en minutos entre dos fechas
* @param date1 fecha inicial.
* @param date2 fecha final.
* @return long correspondiente a diferencia de minutos.
*/

    public long dateDifference(Date date1, Date date2) {
        long diff = date2.getTime() - date1.getTime();
        //play.Logger.info(""+diff);
        return TimeUnit.MILLISECONDS.toSeconds(diff);
    }

/**
* Retorna un valor de verdad si es que el sistema está o no disponible para nuevas
* búsquedas.
*/
    public Result getSystemStatus(){
        if(puedeEjecutar)
            return ok("Libre");
        else{
            String terminosActuales = "";
            for(String s : q.terminos){
                terminosActuales+= s+",";
            }
            return ok("Buscando");
        }
    }

/**
*   Devuelve el calor de cuántos usuarios se han identificado dentro de los marcadores actuales.
*   @return 200 y la cantidad de usuarios.
*/
    public Result getCurrentUsers(){
        long dateOfLastCurrent = getDateOfLastQuery();
        if(dateOfLastCurrent == -1){
            return ok("0");
        }
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection markers = jongo.getCollection("markers");
        MongoCursor<Marker> aM = markers.find().as(Marker.class);
        ArrayList<Marker> aMQ = new ArrayList();
        Date actualDate = new Date();
        while(aM.hasNext()){
            Marker m = aM.next();
            long difference = dateDifference(m.generatedAt, new Date(dateOfLastCurrent));
            if(difference < timeToLive){
                aMQ.add(m);
            }
        }   
        ArrayList<String> idFinded = new ArrayList();
        for(Marker m : aMQ){
            if(idFinded.isEmpty()){
                idFinded.add(m.userID);
            }
            else{
                if(!idFinded.contains(m.userID)){
                    idFinded.add(m.userID);
                }
            }
        }
        return ok(""+idFinded.size());
    }

    /**
*   Devuelve el calor de cuántos necesidades se han identificado dentro de los marcadores actuales.
*   @return 200 y la cantidad de necesidades.
*/
    public Result getCurrentNeeds(){
        long dateOfLastCurrent = getDateOfLastQuery();
        if(dateOfLastCurrent == -1){
            return ok("0");
        }
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection markers = jongo.getCollection("markers");
        MongoCursor<Marker> aM = markers.find().as(Marker.class);
        ArrayList<Marker> aMQ = new ArrayList();
        Date actualDate = new Date();
        while(aM.hasNext()){
            Marker m = aM.next();
            long difference = dateDifference(m.generatedAt, new Date(dateOfLastCurrent));
            if(difference < timeToLive){
                aMQ.add(m);
            }
        }   
        int needCounter = 0;
        for(Marker m : aMQ){
            if(!m.categoria.equals("none"))
                needCounter++;
        }
        return ok(""+needCounter);
    }

/**
*   Devuelve la cantidad de Tweets que el sistema de búsqueda ha encontrado.
*   @result 200 y el valor del tweet.
*/
    public Result getCurrentTweets(){
        
        long dateOfLastCurrent = getDateOfLastQuery();
        if(dateOfLastCurrent == -1){
            return ok("0");
        }
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection markers = jongo.getCollection("markers");
        MongoCursor<Marker> aM = markers.find().as(Marker.class);
        ArrayList<Marker> aMQ = new ArrayList();
        Date actualDate = new Date();
        while(aM.hasNext()){
            Marker m = aM.next();
            aMQ.add(m);
        }   
        int needCounter = 0;
        for(Marker m : aMQ){
            needCounter++;                
        }
        return ok(""+needCounter);
    }

    private long getDateOfLastQuery(){
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection queries = jongo.getCollection("queries");
        MongoCursor<Query> aQ = queries.find().as(Query.class);
        Query lastQuery = new Query();
        boolean finded = false;
        while(aQ.hasNext()){
            lastQuery = aQ.next();
            if(lastQuery.estado.equals("actual"))
            {
                finded = true;
                break;
            }
        }
        if(finded){
            return lastQuery.generatedAt.getTime();
        }
        else{
            return -1;
        }

    }

/**
* Fines de testeo. Pobla la base de datos con datos aleatoreos.
*/
    public Result populateTestDataBase(){
        DB db = new MongoClient().getDB(dataBaseName);
        Jongo jongo = new Jongo(db);
        MongoCollection markers = jongo.getCollection("markers");
        List<String> cat = new ArrayList();
        Random rn = new Random();
        cat.add("agua");
        cat.add("alimento");
        cat.add("electricidad");
        cat.add("comunicacion");
        cat.add("personas");
        cat.add("seguridad");
        cat.add("none");
        List<String> cont = new ArrayList();
        cont.add("Lorem ipsum dolor sit amet");
        cont.add("consectetur adipiscing elit");
        cont.add("sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
        cont.add("Ut enim ad minim veniam");
        cont.add("quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat");
        int counter = 0;
        for(int i = 0; i < 20; i++){
            Marker m = new Marker();
            m.latitud = (rn.nextDouble()*70);
            m.longitud = (rn.nextDouble()*70);
            m.categoria = cat.get(i%7);
            m.contenido = cont.get(i%5);;
            m.generatedAt = new Date();
            m.userID = ""+Math.abs(rn.nextLong()*10);
            markers.save(m);
            counter = i+1;
        }
        return ok("Agregados "+ counter +" registros.");
    }

    

    public Result updateModel() throws FileNotFoundException, IOException, ClassNotFoundException{
        String fileContent = "";
        
        MultipartFormData<File> body = request().body().asMultipartFormData();
        FilePart<File> data = body.getFile("rawData");

        File f = data.getFile();

        

        MalletUtility mU = new MalletUtility();
        BufferedReader br = new BufferedReader(new FileReader(f));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            fileContent = sb.toString();
        } finally {
            br.close();
        }
        ArrayList<InstanceUtil> iU = mU.receiveFile(fileContent);
        iU = mU.cleanData(iU);
        InstanceList iL = mU.prepareInstance(iU);
        Classifier viejoClasificador = mU.readClassifier();
        //String previousAccuracy = ""+mU.evaluate(viejoClasificador, iU);
        Classifier nuevoClassificador = mU.createNBClassifier(iL);
        //String currentAccuracy = ""+mU.evaluate(nuevoClassificador, iU);
        ArrayList<String> judge = mU.judgeClassifier(viejoClasificador, nuevoClassificador, iU);
        String modelResult = "";
        String userMsj = "";
        String value1 = "";
        String value2 = "";
        if(judge.isEmpty()){
            return ok(modelUpdated.render("No se ha encontrado modelo","0.00","0.00"));
        }
        else{

            switch(judge.get(0)){
                case "new":
                    userMsj = "El nuevo modelo tiene mayor precisión. Se utilizará.";
                    break;
                case "old":
                    userMsj = "El modelo antiguo tiene la misma o mayor precisión. Se mantendrá.";
                    break;
                default:
                    userMsj = "No se ha encontrado modelo.";
                    break;
            }
            modelResult = judge.get(0)+";"+judge.get(1)+";"+judge.get(2);
        }
        if(judge.get(0).equals("new")){
           mU.saveClassifier(nuevoClassificador); 
        }
        value1 = ""+round(Double.parseDouble(judge.get(1))*100,2);
        value2 = ""+round(Double.parseDouble(judge.get(2))*100,2);
        return ok(modelUpdated.render(userMsj, value1, value2));
    }



    public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
}

}


