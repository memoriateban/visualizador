
var histogramData = (function () {
  var histogramData = null;
  $.ajax({
    'async': false,
    'global': false,
    'url': "getDateRangeLocations/"+new Date(2010, 0, 1).getTime()+"/"+new Date(2018, 11, 31).getTime()+"",
    'dataType': "json",
    'success': function (data) {
      //console.log(data);
      histogramData = data;
    }
  });
  return histogramData;
})();


/**
* Devuelve cuantos elementos existen de cada fecha en la data total.
*/
function getFrequencies(){
  var indexOfElement = 0;
  var frequencyTable = new Array();
  //frequencyTable.push(["Fecha","Eventos"])
  $.each(histogramData, function(index, value){
    var dataDate = hGetDate(value.generatedAt);
    /*var insertedData = new Array();
    insertedData.push(dataDate);*/
    if(frequencyTable.length != 0){
      if(existe(frequencyTable, dataDate)){
        for(var i = 0; i < frequencyTable.length; i++){
          if(frequencyTable[i][0] == dataDate){
            frequencyTable[i][1] += 1;
            break;
          }
        }
      }
      else{
        frequencyTable.push([dataDate, 1]);
      }
    }
    else{
      frequencyTable.push([dataDate, 1]);
    }
  });
  return frequencyTable;
}

function existe(arreglo, objeto){
  for(var i = 0; i < arreglo.length; i++){
    if(arreglo[i][0] == objeto){
      return true;
    }
  }
  return false;
}

/**
* Convierte un epoch time a date con formato dd/MM/yyyy
*/

function  transformToDate(frecTable){

  var result = new Array();
  for(var i = 0; i < frecTable.length; i++){
    var date = new Date(frecTable[i][0]);    
    var counter = frecTable[i][1];
    result.push([date, counter]);
  }
  result.push([new Date(2010, 0, 1), 0]);
  result.push([new Date(2018, 11, 31), 0]);

  return result;
}

function  transformToEpoch(frecTable){

  var result = new Array();
  result.push([(new Date(2015, 0, 1)).getTime(), 0]);
  for(var i = 0; i < frecTable.length; i++){
    var date = (new Date(frecTable[i][0])).getTime();    
    var counter = frecTable[i][1];
    result.push([date, counter]);
  }
  
  result.push([(new Date(2018, 11, 31)).getTime(), 0]);

  return result;
}

function getDataForHighStock(){
  return transformToEpoch(getFrequencies());
}

function sortData(){
  var data = getDataForHighStock();

}

function hGetDate(epochTime){
  var date = new Date(epochTime);
  return (date.getDay()+1)+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
}

/*function drawChart() {
  var data = new google.visualization.DataTable();
  data.addColumn('date', 'Fecha');
  data.addColumn('number', 'Eventos');
  data.addRows(transformToDate(getFrequencies()));
          
  var chartOptions = {
    displayAnnotations: false,
    dateFormat: 'dd, MMMM of yyyy'
  };
  var chart = new google.visualization.AnnotatedTimeLine(document.getElementById('histograma'));
  chart.draw(data, chartOptions);
}

google.charts.load('current', {'packages':['annotatedtimeline']});
google.charts.setOnLoadCallback(drawChart);
//console.log(transformToEpoch(getFrequencies()));      
*/

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

$(document).ready(function(){
        var data = getDataForHighStock();
        data = sortByKey(data,0)
        var dataObject = {
            rangeSelector: {
                enabled:true,
                selected: 2
            },

            rangeSelector:{
              selected: 5,
              enabled: true
            },
            
            title: {
                text: 'Cantidad de eventos por fecha'
            },
            
            series: [{
                type: 'column',
                name: 'Cantidad de eventos',
                data: data,
                tooltip: {
                    valueDecimals: 0
                }
            }],
            
            chart: {
                renderTo: 'histograma2'
            },

            xAxis: {
                events: {
                  setExtremes: function (e) {
                    valuesOfAxis[0] = Highcharts.dateFormat(null, e.min);
                    valuesOfAxis[1] = Highcharts.dateFormat(null, e.max);
                    //console.log(Highcharts.dateFormat(null, e.min));
                    //console.log(Highcharts.dateFormat(null, e.max));
                    //console.log(valuesOfAxis);
                    $('#histograma2').trigger('change');
                    }
            }
        },
            
        };
        
        Highcharts.createElement('link', {
   href: 'https://fonts.googleapis.com/css?family=Unica+One',
   rel: 'stylesheet',
   type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);

Highcharts.theme = {

  lang: {
    months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',  'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre', 'Diciembre'],
    weekdays: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    downloadJPEG: 'Descargar JPEG',
    downloadPDF: 'Descargar PDF',
    downloadPNG: 'Descargar PNG',
    downloadSVG: 'Descargar SVG',
    invalidDate: 'Fecha inválida',
    loading: 'Cargando',
    printChart: 'Imprimir gráfico',
    rangeSelectorFrom: 'Desde',
    rangeSelectorTo: 'Hasta',
    shortMonths: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    shortWeekdays: ['dom', 'lun', 'mar', 'mie', 'jue', 'vie', 'sab']
  }, 
   colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
   chart: {
      backgroundColor: {
         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, '#2a2a2b'],
            [1, '#3e3e40']
         ]
      },
      style: {
         fontFamily: "'Unica One', sans-serif"
      },
      plotBorderColor: '#606063'
   },
   title: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase',
         fontSize: '20px'
      }
   },
   subtitle: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase'
      }
   },
   xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
      Highcharts.setOptions(Highcharts.theme);

      var chart = new Highcharts.StockChart(dataObject);  

       /* var extremes = chart.xAxis[0].getExtremes(),
        start = new Date(extremes.min);
        end   = new Date(extremes.max); 
        console.log(start);
        console.log(end);*/
});


