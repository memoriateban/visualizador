
  /**

  NO USADO!!


    Datos de los marcadores
  */
  var currentMarkerData;

  /**
    Cluster Común
  */
  var mc;
  /**
    Clusters por categorías
  */
  var mcWater;
  var mcFood;
  var mcElect;
  var mcComun;
  var mcPeople;
  var mcSafety;

  /**
    Valor de qué tipo de cluster se está utilizando
  */
  var clusterType;

  /**
    Marcador de inicio. Informativo.
  */
  var locs = {
      1: {
          contenido: 'La aplicación se está iniciando.<br> En breve se actualizaran los marcadores.',
          categoria: 'none',
          latitud: -33.45,
          longitud: -70.666667
      }
  };

  /**
    Inicializa el mapa.
  */
  var map = new google.maps.Map(document.getElementById('map_2385853'), {
      zoom: 4,
      maxZoom: 16,
      minZoom: 1,
      streetViewControl: false,
      center: new google.maps.LatLng(-37.559706648411975, -72.64208984375),
      mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  /**
    Inicializa ventana para mostrar Tweets.
  */
  var infowindow = new google.maps.InfoWindow();

  /**
    Inicia un arreglo donde se guardaran los marcadores.
  */
  var gmarkers = new Array();

  /**
    Inicia las opciones del cluster y les pasa sus respectivos estilos (imágenes).
  */
  var mcOptions = {gridSize: 50, maxZoom: 15 /*Default Image Style*/};
  var mcWaterOptions = {styles: [
  {
    textColor: 'white',
    url_: '/assets/images/aguaS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url_: '/assets/images/aguaM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url_: '/assets/images/aguaL.png',
    height: 40,
    width: 40
  }
]};
  var mcFoodOptions = {styles: [
  {
    textColor: 'white',
    url: '/assets/images/alimentoS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/alimentoM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/alimentoL.png',
    height: 40,
    width: 40
  }
]};
  var mcElectOptions = {styles: [
  {
    textColor: 'white',
    url: '/assets/images/electricidadS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/electricidadM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/electricidadL.png',
    height: 40,
    width: 40
  }
]};
  var mcPeopleOptions = {styles: [
  {
    textColor: 'white',
    url: '/assets/images/personasS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/personasM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/personasL.png',
    height: 40,
    width: 40
  }
]};
  var mcSafetyOptions = {styles: [
  {
    textColor: 'white',
    url: '/assets/images/seguridadS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/seguridadM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/seguridadL.png',
    height: 40,
    width: 40
  }
]};
  var mcComunOptions = {styles: [
  {
    textColor: 'white',
    url: '/assets/images/comunicacionS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/comunicacionM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/comunicacionL.png',
    height: 40,
    width: 40
  }
]};


  /**
    Inicializa todos los clusters al mapa. (Cluster vacío es necesario por parámetro)
  */
  mc = new MarkerClusterer(map, mcOptions);
  var mcWater = new MarkerClusterer(map, gmarkers, mcWaterOptions);
  var mcFood = new MarkerClusterer(map, gmarkers, mcFoodOptions);
  var mcElect = new MarkerClusterer(map, gmarkers, mcElectOptions);
  var mcComun = new MarkerClusterer(map, gmarkers, mcComunOptions);
  var mcPeople = new MarkerClusterer(map, gmarkers, mcPeopleOptions);
  var mcSafety = new MarkerClusterer(map, gmarkers, mcSafetyOptions);



  /**
    Función que setea los marcadores a los clusters que correspondan. 
    IMPORTANTE: En primer lugar SIEMPRE ha de eliminar los marcadores de los clusters antiguos.
  */
  function setMarkers(locObj) {  
      var categoryFilter = document.getElementById('categoryFilter').value; 
      clusterType = document.getElementById('clusterType').value; 
      clearOverlays();
      $.each(locObj, function (key, loc) {
        if(loc.categoria!="none"){
          if(categoryFilter=="all"){
            /*
              Utiliza cada elemento para hacer un marcador utilizando la ubicacion y
              la categoría. Caso en que se muestran todos.
            */
            loc.marker = new google.maps.Marker({
                  position: new google.maps.LatLng(loc.latitud, loc.longitud),
                  icon: '/assets/images/'+loc.categoria+'.png',          
                  map: map
              });

            google.maps.event.addListener(loc.marker, 'click', (function (key) {
                      return function () {
                          infowindow.setContent("<span style=\"color: black;\"><b style=\"color: black;\">Tweet</b>: (Categoría: "+loc.categoria+")<br> "+loc.contenido+"</span>");
                          infowindow.open(map, loc.marker);
                          map.setZoom(3);
                          map.setCenter(marker.getPosition());
                      }
                  })(key));
            gmarkers.push(loc.marker);
          }
          else if(loc.categoria == categoryFilter){

            /*
              Utiliza cada elemento para hacer un marcador utilizando la ubicacion y
              la categoría. Caso en el que se muestra por categoría.
            */
            loc.marker = new google.maps.Marker({
                  position: new google.maps.LatLng(loc.latitud, loc.longitud),
                  icon: '/assets/images/'+loc.categoria+'.png',          
                  map: map
              });

            /*
              A cada marcador le asocia un InfoWindows donde se muestra el Tweet.
            */
            google.maps.event.addListener(loc.marker, 'click', (function (key) {
                      return function () {
                          infowindow.setContent("<span style=\"color: black;\"><b style=\"color: black;\">Tweet</b>: (Categoría: "+loc.categoria+")<br> "+loc.contenido+"</span>");
                          infowindow.open(map, loc.marker);
                          map.setZoom(3);
                          map.setCenter(marker.getPosition());
                      }
                  })(key));
            gmarkers.push(loc.marker);
          }
        }
      });
      
      /*
        Tres casos:
          - No agrupar nada, muestra los cluster tal cual.
          - Agrupar todos, agrupa según la distancia.
          - Agrupa según la categoría sólo esos elementos.
      */
      if(clusterType == "all"){
        mc.addMarkers(gmarkers);
      }
      else if(clusterType == "none"){
        //No agrupar.
      }
      else{
        var WaterMarkers = new Array();
        var FoodMarkers = new Array();
        var ElectMarkers = new Array();
        var SafetyMarkers = new Array();
        var ComunMarkers = new Array();
        var PeopleMarkers = new Array();
        for(var i = 0; i < gmarkers.length; i++){
          /* 
            Obtiene el nombre de la categoría del ícono asociado 
          */
          var locationString = gmarkers[i].icon;
          locationString = locationString.replace("/assets/images/","");
          locationString = locationString.replace(".png","");
          switch(locationString){
            case "agua":
              WaterMarkers.push(gmarkers[i]);
              break;
            case "alimento":
              FoodMarkers.push(gmarkers[i]);
              break;
            case "electricidad":
              ElectMarkers.push(gmarkers[i]);
              break;
            case "seguridad":
              SafetyMarkers.push(gmarkers[i]);
              break;
            case "personas":
              PeopleMarkers.push(gmarkers[i]);
              break;
            case "comunicacion":
              ComunMarkers.push(gmarkers[i]);
              break;
            } 
          }
        mcWater.addMarkers(WaterMarkers);
        mcFood.addMarkers(FoodMarkers);
        mcElect.addMarkers(ElectMarkers);
        mcSafety.addMarkers(SafetyMarkers);
        mcComun.addMarkers(ComunMarkers);
        mcPeople.addMarkers(PeopleMarkers);
      }
      cleanInfoMarker();  
}

/**
  Funcion que limpia todos los marcadores y clusters del mapa.
*/
function clearOverlays() {
 while(gmarkers.length) { gmarkers.pop().setMap(null); }
  gmarkers.length = 0;
  mc.clearMarkers();
  mcWater.clearMarkers();
  mcFood.clearMarkers();
  mcElect.clearMarkers();
  mcSafety.clearMarkers();
  mcComun.clearMarkers();
  mcPeople.clearMarkers();
}

/**
  Elimina el marcador informativo.
*/
function cleanInfoMarker() {
 while(locs.length) { locs.pop().setMap(null); }
  locs.length = 0;
}

  /**
    Llamada inicial al sistema. Setea el Marcador Informativo.
  */
  setMarkers(locs);

  /**
    Obtiene el tiempo de refresco seteado en el sistema. 
    Además obtiene la nueva información de marcadores.
  */
  var delay = (function () {
      var delay = null;
      $.ajax({
            'async': false,
            'global': false,
            'url': "/config/getRefreshTime",
            'dataType': "json",
            'success': function (data) {
                delay = data;
            }
        });
      return delay*1000;
    })(); 

  setInterval(function () {
    var json = (function () {
      var json = null;
      $.ajax({
          'async': false,
          'global': false,
          'url': "/getLocations",
          'dataType': "json",
          'success': function (data) {
              json = data;
          }
      });
      return json;
    })(); 
    currentData = json;
    //console.log(json);
    setMarkers(json); 
     
  }, delay);

  /**
    Función invocada al cambiar tipo de categoría. Vuelve a acomodar los marcadores.
  */
  function filterChanged(){
    try{
      setMarkers(currentData);
    }
    catch(e){
      console.log("La información de los marcadores no está lista aun.");
    }
      
  }
