var tweetCount = 0;

function getCurrentTweets(){
	$.getJSON("/getLocations", function(data){
		$('#tweetsProcesados').html();
		tweetCount = data.length;

	    $.each(data, function (index, value) {
	        //console.log(value.contenido);
	        var currentTime = new Date(value.generatedAt)
	        var month = currentTime.getMonth() + 1
	        var day = currentTime.getDate()
	        var year = currentTime.getFullYear()
	        var date = day + " de " + month + " del " + year
	        $('#tweetsProcesados').append('<tr><td>'+value.contenido+'</td><td>'+date+'</td></tr>');

	    });
	    $('#contadorTweets').html('<b>Tweets procesados: </b>'+ tweetCount);
	    getCurrentTweets();
	});
}
$('#actualizarRegistroDeTweets').click(function() {
	getCurrentTweets();
});
