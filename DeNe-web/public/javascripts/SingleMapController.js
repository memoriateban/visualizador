
  /**
    NO USADO

    Datos de los marcadores
  */
  var currentMarkerData;

  /**
    Cluster Común
  */
  var mc;
  /**
    Clusters por categorías
  */
  var mcWater;
  var mcFood;
  var mcElect;
  var mcComun;
  var mcPeople;
  var mcSafety;

  /**
    Valor de qué tipo de cluster se está utilizando y qué filtro se está aplicando.
  */
  var clusterType = document.getElementById('clusterType').value; 
  var categoryFilter = document.getElementById('categoryFilter').value; 

  /**
    Marcador de inicio. Informativo.
  */
  var locs = {
      1: {
          contenido: 'La aplicación se está iniciando.<br> En breve se actualizaran los marcadores.',
          categoria: 'none',
          latitud: -33.45,
          longitud: -70.666667
      }
  };

  /**
    Inicializa el mapa.
  */
  var map = new google.maps.Map(document.getElementById('map_2385853'), {
      zoom: 4,
      maxZoom: 16,
      minZoom: 1,
      streetViewControl: false,
      center: new google.maps.LatLng(-37.559706648411975, -72.64208984375),
      mapTypeId: google.maps.MapTypeId.TERRAIN
  });

  /**
    Inicializa todos los clusters al mapa.
  */
  mc = new MarkerClusterer(map);
  var mcWater = new MarkerClusterer(map);
  var mcFood = new MarkerClusterer(map);
  var mcElect = new MarkerClusterer(map);
  var mcComun = new MarkerClusterer(map);
  var mcPeople = new MarkerClusterer(map);
  var mcSafety = new MarkerClusterer(map);

  /**
    Inicializa ventana para mostrar Tweets.
  */
  var infowindow = new google.maps.InfoWindow();

  /**
    Inicia un arreglo donde se guardaran los marcadores.
  */
  var gmarkers = new Array();

  /**
    Imágenes para cada tipo de cluster.
  */
  var WaterStyle = [];
  var FoodStyle = [];
  var ElectStyle = [];
  var PeopleStyle = [];
  var SafetyStyle = [];
  var ComunStyle = [];

  /**
    Inicia las opciones del cluster y les pasa sus respectivos estilos (imágenes).
  */
  var mcOptions = {gridSize: 50, maxZoom: 15};
  var mcWaterOptions = {gridSize: 50, maxZoom: 15};
  var mcFoodOptions = {gridSize: 50, maxZoom: 15};
  var mcElectOptions = {gridSize: 50, maxZoom: 15};
  var mcPeopleOptions = {gridSize: 50, maxZoom: 15};
  var mcSafetyOptions = {gridSize: 50, maxZoom: 15};
  var mcComunOptions = {gridSize: 50, maxZoom: 15};

  /**
    Setea los valores los límites del DateSlider.
  */
  var values = $("#slider").dateRangeSlider().dateRangeSlider("values");
  $("#slider").dateRangeSlider("bounds", new Date(2010, 0, 1), new Date(2018, 11, 31));
  $("#slider").dateRangeSlider("values", new Date(2016, 0, 1), new Date(2017, 0, 31));

function setMarkers(locObj) {  
      clearOverlays();
      $.each(locObj, function (key, loc) {
        if(loc.categoria!="none"){
          if(categoryFilter=="all"){
            /*
              Utiliza cada elemento para hacer un marcador utilizando la ubicacion y
              la categoría. Caso en que se muestran todos.
            */
            loc.marker = new google.maps.Marker({
                  position: new google.maps.LatLng(loc.latitud, loc.longitud),
                  icon: '/assets/images/'+loc.categoria+'.png',          
                  map: map
              });

            google.maps.event.addListener(loc.marker, 'click', (function (key) {
                      return function () {
                          infowindow.setContent("<span style=\"color: black;\"><b style=\"color: black;\">Tweet</b>: (Categoría: "+loc.categoria+")<br> "+loc.contenido+"</span>");
                          infowindow.open(map, loc.marker);
                          map.setZoom(3);
                          map.setCenter(marker.getPosition());
                      }
                  })(key));
            gmarkers.push(loc.marker);
          }
          else if(loc.categoria == categoryFilter){

            /*
              Utiliza cada elemento para hacer un marcador utilizando la ubicacion y
              la categoría. Caso en el que se muestra por categoría.
            */
            loc.marker = new google.maps.Marker({
                  position: new google.maps.LatLng(loc.latitud, loc.longitud),
                  icon: '/assets/images/'+loc.categoria+'.png',          
                  map: map
              });

            /*
              A cada marcador le asocia un InfoWindows donde se muestra el Tweet.
            */
            google.maps.event.addListener(loc.marker, 'click', (function (key) {
                      return function () {
                          infowindow.setContent("<span style=\"color: black;\"><b style=\"color: black;\">Tweet</b>: (Categoría: "+loc.categoria+")<br> "+loc.contenido+"</span>");
                          infowindow.open(map, loc.marker);
                          map.setZoom(3);
                          map.setCenter(marker.getPosition());
                      }
                  })(key));
            gmarkers.push(loc.marker);
          }
        }
      });
      
      /*
        Tres casos:
          - No agrupar nada, muestra los cluster tal cual.
          - Agrupar todos, agrupa según la distancia.
          - Agrupa según la categoría sólo esos elementos.
      */
      if(clusterType == "all"){
        mc.addMarkers(gmarkers);
      }
      else if(clusterType == "none"){
        //No agrupar.
      }
      else{
        var WaterMarkers = new Array();
        var FoodMarkers = new Array();
        var ElectMarkers = new Array();
        var SafetyMarkers = new Array();
        var ComunMarkers = new Array();
        var PeopleMarkers = new Array();
        for(var i = 0; i < gmarkers.length; i++){
          /* 
            Obtiene el nombre de la categoría del ícono asociado 
          */
          var locationString = gmarkers[i].icon;
          locationString = locationString.replace("/assets/images/","");
          locationString = locationString.replace(".png","");
          switch(locationString){
            case "agua":
              WaterMarkers.push(gmarkers[i]);
              break;
            case "alimento":
              FoodMarkers.push(gmarkers[i]);
              break;
            case "electricidad":
              ElectMarkers.push(gmarkers[i]);
              break;
            case "seguridad":
              SafetyMarkers.push(gmarkers[i]);
              break;
            case "personas":
              PeopleMarkers.push(gmarkers[i]);
              break;
            case "comunicacion":
              ComunMarkers.push(gmarkers[i]);
              break;
            } 
          }
        mcWater.addMarkers(WaterMarkers);
        mcFood.addMarkers(FoodMarkers);
        mcElect.addMarkers(ElectMarkers);
        mcSafety.addMarkers(SafetyMarkers);
        mcComun.addMarkers(ComunMarkers);
        mcPeople.addMarkers(PeopleMarkers);
      }
      cleanInfoMarker();  
}

/**
* Se llama al cambiar algun filtro en el mapa.
* Posiciona los marcadores / clusters según el filtro seleccionado.
*/
function filterChanged(){
  try{
    setMarkers(currentData);
  }
  catch(e){
    console.log("La información de los marcadores no está lista aun.");
  }
}

/**
  Funcion que limpia todos los marcadores y clusters del mapa.
*/
function clearOverlays() {
  while(gmarkers.length) { gmarkers.pop().setMap(null); }
    gmarkers.length = 0;
    mc.clearMarkers();
    mcWater.clearMarkers();
    mcFood.clearMarkers();
    mcElect.clearMarkers();
    mcSafety.clearMarkers();
    mcComun.clearMarkers();
    mcPeople.clearMarkers();
}

/**
  Elimina el marcador informativo.
*/
function cleanInfoMarker() {
  while(locs.length) { locs.pop().setMap(null); }
    locs.length = 0;
}


/** BLOQUE PRINCIPAL DEL CÓDIGO JS*/

/**
  Obtiene el tiempo de refresco seteado en el sistema. 
  Además obtiene la nueva información de marcadores.
*/
  var delay = (function () {
      var delay = null;
      $.ajax({
            'async': false,
            'global': false,
            'url': "/config/getRefreshTime",
            'dataType': "json",
            'success': function (data) {
                delay = data;
            }
        });
      return delay*1000; //Tiempo en segundos.
    })(); 

/**
  Llamada inicial al sistema. Setea el Marcador Informativo.
*/
setMarkers(locs);

/**
  Inicia el loop cada (Delay*1000) segundos y obtiene los nuevos marcadores dentro del intervalo seleccionado
*/
setInterval(function () {
    var dateSliderVal = $("#slider").dateRangeSlider("values");
    var json = (function () {
      var json = null;
      $.ajax({
          'async': false,
          'global': false,
          'url': "getDateRangeLocations/"+new Date(dateSliderVal.min).getTime()+"/"+new Date(dateSliderVal.max).getTime()+"",
          'dataType': "json",
          'success': function (data) {
              json = data;
          }
      });
      return json;
    })(); 
    currentData = json;
    //console.log(json);
    setMarkers(json); 
     
  }, delay);

/**
  Captura los cambios del intervalo de tiempo y actualiza marcadores y clusters.
*/

$("#slider").bind("valuesChanging", function(e, data){
   
  var json = (function () {
      var json = null;
      $.ajax({
          'async': false,
          'global': false,
          'url': "getDateRangeLocations/"+new Date(data.values.min).getTime()+"/"+new Date(data.values.max).getTime()+"",
          'dataType': "json",
          'success': function (data) {
              json = data;
          }
      });
      return json;
    })(); 
    currentMarkerData = json;
    setMarkers(json);
     
})