	/**
    Contenedor de los valores del histograma
  */
  var valuesOfAxis = new Array();
  valuesOfAxis.push(-1);
  valuesOfAxis.push(-1);
  /**
    Clusters para cada categoría.
  */
  var hmc;
  var hmcWater;
  var hmcFood;
  var hmcElect;
  var hmcComun;
  var hmcPeople;
  var hmcSafety;

  /**
    Reciben qué filtros se están usando.
  */
  var categoryFilterHistorico;
  var clusterTypeFilterHistorico;

  /**
    Inicia un arreglo donde se guardaran los marcadores.
  */
	var historicalLocations = new Array();

  /**
    Guarda la data recuperada por REST
  */
	var currentHistoricalMarkerData = new Array();

  /**
    Caracterización de cada cluster
  */
	var hmcOptions = {gridSize: 50, maxZoom: 15 /*Default Image Style*/};
  var hmcWaterOptions = {gridSize: 50, maxZoom: 15, styles: [
  {
    textColor: 'white',
    url: '/assets/images/aguaS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/aguaM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/aguaL.png',
    height: 40,
    width: 40
  }
]};
  var hmcFoodOptions = {gridSize: 50, maxZoom: 15, styles: [
  {
    textColor: 'white',
    url: '/assets/images/alimentoS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/alimentoM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/alimentoL.png',
    height: 40,
    width: 40
  }
]};
  var hmcElectOptions = {gridSize: 50, maxZoom: 15, styles: [
  {
    textColor: 'white',
    url: '/assets/images/electricidadS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/electricidadM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/electricidadL.png',
    height: 40,
    width: 40
  }
]};
  var hmcPeopleOptions = {gridSize: 50, maxZoom: 15, styles: [
  {
    textColor: 'white',
    url: '/assets/images/personasS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/personasM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/personasL.png',
    height: 40,
    width: 40
  }
]};
  var hmcSafetyOptions = {gridSize: 50, maxZoom: 15, styles: [
  {
    textColor: 'white',
    url: '/assets/images/seguridadS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/seguridadM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/seguridadL.png',
    height: 40,
    width: 40
  }
]};
  var hmcComunOptions = {gridSize: 50, maxZoom: 15, styles: [
  {
    textColor: 'white',
    url: '/assets/images/comunicacionS.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/comunicacionM.png',
    height: 40,
    width: 40
  },
 {
    textColor: 'white',
    url: '/assets/images/comunicacionL.png',
    height: 40,
    width: 40
  }
]};

  /**
    Inicializa el mapa.
  */
	var historicalMap = new google.maps.Map(document.getElementById('mapHistorico'),{
		zoom: 4,
	    maxZoom: 14,
	    minZoom: 1,
	    streetViewControl: false,
	    center: new google.maps.LatLng(-33.45, -70.666667),
	    mapTypeId: google.maps.MapTypeId.TERRAIN 
	 });
	
  /**
    Inicializa cada cluster con sus respectivas opciones.
  */
  hmc = new MarkerClusterer(historicalMap, historicalLocations, hmcOptions);
  hmcWater = new MarkerClusterer(historicalMap, historicalLocations, hmcWaterOptions);
  hmcFood = new MarkerClusterer(historicalMap, historicalLocations, hmcFoodOptions);
  hmcElect = new MarkerClusterer(historicalMap, historicalLocations, hmcElectOptions);
  hmcComun = new MarkerClusterer(historicalMap, historicalLocations, hmcComunOptions);
  hmcPeople = new MarkerClusterer(historicalMap, historicalLocations, hmcPeopleOptions);
  hmcSafety = new MarkerClusterer(historicalMap, historicalLocations, hmcSafetyOptions);
	
  /**
    Inicializa la barra de fechas.
  */
  $("#slider").dateRangeSlider().dateRangeSlider("values");
	$("#slider").dateRangeSlider("bounds", new Date(2010, 0, 1), new Date(2018, 11, 31));
  $("#slider").dateRangeSlider("values", new Date(2016, 2, 1), new Date(2016, 7, 31));
  

function setHistoricalMarkers(locObj) {
	removeHistoricalMarkers(historicalLocations);
	categoryFilterHistorico = document.getElementById('categoryFilterHistorico').value;
  clusterTypeFilterHistorico = document.getElementById('TypeFilterHistorico').value;
      $.each(locObj, function (key, loc) {
        if(loc.categoria!="None" && loc.categoria!=""){
        	if(categoryFilterHistorico == "all"){
        		loc.marker = new google.maps.Marker({
              position: new google.maps.LatLng(loc.latitud, loc.longitud),
              icon: '/assets/images/'+loc.categoria+'.png',          
              map: historicalMap,
              clickable: true
            });
            loc.marker.info = new google.maps.InfoWindow({
              content:  '<div id="iw-container">' +
                            '<div class="iw-title" style="text-align:center;">Tweet</div>' +
                          '<div class="iw-content">' +
                            '<div class="iw-subTitle">Categoría identificada: '+loc.categoria+'</div>' +
                            '<br>'+
                            '<p>'+loc.contenido+'</p>'+
                          '</div>' +
                          '<div class="iw-bottom-gradient"></div>' +
                        '</div>',
                        maxWidth: 350
            });
                  google.maps.event.addListener(loc.marker, 'click', function() {
                    loc.marker.info.open(historicalMap, loc.marker);
                  });

                  google.maps.event.addListener(loc.marker.info, 'domready', function() {
                    var iwOuter = $('.gm-style-iw');
                    var iwBackground = iwOuter.prev();
                    iwBackground.children(':nth-child(2)').css({'display' : 'none'});
                    iwBackground.children(':nth-child(4)').css({'display' : 'none'});
                    iwOuter.parent().parent().css({left: '115px'});
                    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
                    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
                    var iwCloseBtn = iwOuter.next();
                    iwCloseBtn.css({
                    opacity: '1', // by default the close button has an opacity of 0.7
                    right: '38px', top: '3px', // button repositioning
                    border: '15px solid #48b5e9', // increasing button border and new color
                    'border-radius': '13px', // circular effect
                    'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
                    });
                    if($('.iw-content').height() < 140){
                      $('.iw-bottom-gradient').css({display: ''});
                    }
                    iwCloseBtn.mouseout(function(){
                      $(this).css({opacity: '1'});
                    });
                  });

        		/*google.maps.event.addListener(loc.marker, 'click', (function (key) {
               		return function () {
                    	infowindow.setContent("<span style=\"color: black;\"><b style=\"color: black;\">Tweet</b>: (Categoría: "+loc.categoria+")<br> "+loc.contenido+"</span>");
                      	infowindow.open(historicalMap, loc.marker);
                      	map.setZoom(3);
                      	map.setCenter(marker.getPosition());
                	}
             	})(key));*/
              historicalLocations.push(loc.marker);
        	}
        	else if(loc.categoria == categoryFilterHistorico){
        		loc.marker = new google.maps.Marker({
             	position: new google.maps.LatLng(loc.latitud, loc.longitud),
             	icon: '/assets/images/'+loc.categoria+'.png',          
             	map: historicalMap
            });
        		loc.marker.info = new google.maps.InfoWindow({
              content:  '<div id="iw-container">' +
                            '<div class="iw-title" style="text-align:center;">Tweet</div>' +
                          '<div class="iw-content">' +
                            '<div class="iw-subTitle">Categoría identificada: '+loc.categoria+'</div>' +
                            '<br>'+
                            '<p>'+loc.contenido+'</p>'+
                          '</div>' +
                          '<div class="iw-bottom-gradient"></div>' +
                        '</div>',
                        maxWidth: 350
            });

            google.maps.event.addListener(loc.marker, 'click', function() {
              loc.marker.info.open(historicalMap, loc.marker);
            });

            google.maps.event.addListener(loc.marker.info, 'domready', function() {
              var iwOuter = $('.gm-style-iw');
              var iwBackground = iwOuter.prev();
              iwBackground.children(':nth-child(2)').css({'display' : 'none'});
              iwBackground.children(':nth-child(4)').css({'display' : 'none'});
              iwOuter.parent().parent().css({left: '115px'});
              iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
              iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});
              iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
              var iwCloseBtn = iwOuter.next();
              iwCloseBtn.css({
              opacity: '1', // by default the close button has an opacity of 0.7
              right: '38px', top: '3px', // button repositioning
              border: '15px solid #48b5e9', // increasing button border and new color
              'border-radius': '13px', // circular effect
              'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
              });
              if($('.iw-content').height() < 140){
                $('.iw-bottom-gradient').css({display: ''});
              }
              iwCloseBtn.mouseout(function(){
                $(this).css({opacity: '1'});
              });
            });

        		historicalLocations.push(loc.marker);
        	}
        }
    });
    //hmc.addMarkers(historicalLocations);

    if(clusterTypeFilterHistorico == "all"){
        hmc.addMarkers(historicalLocations);
    }
    else if(clusterTypeFilterHistorico == "none"){
        //No agrupar.
    }
    else{
        var hWaterMarkers = new Array();
        var hFoodMarkers = new Array();
        var hElectMarkers = new Array();
        var hSafetyMarkers = new Array();
        var hComunMarkers = new Array();
        var hPeopleMarkers = new Array();
        for(var i = 0; i < historicalLocations.length; i++){
          /* 
            Obtiene el nombre de la categoría del ícono asociado 
          */
          var locationString = historicalLocations[i].icon;
          locationString = locationString.replace("/assets/images/","");
          locationString = locationString.replace(".png","");
          switch(locationString){
            case "Agua":
              hWaterMarkers.push(historicalLocations[i]);
              break;
            case "Alimento":
              hFoodMarkers.push(historicalLocations[i]);
              break;
            case "Electricidad":
              hElectMarkers.push(historicalLocations[i]);
              break;
            case "Seguridad":
              hSafetyMarkers.push(historicalLocations[i]);
              break;
            case "Personas":

              hPeopleMarkers.push(historicalLocations[i]);
              break;
            case "Comunicación":
              hComunMarkers.push(historicalLocations[i]);
              break;
            } 
          }
        hmcWater.addMarkers(hWaterMarkers);
        hmcFood.addMarkers(hFoodMarkers);
        hmcElect.addMarkers(hElectMarkers);
        hmcSafety.addMarkers(hSafetyMarkers);
        hmcComun.addMarkers(hComunMarkers);
        hmcPeople.addMarkers(hPeopleMarkers);
      }

}

function removeHistoricalMarkers(markerArray){
    for(i=0; i<markerArray.length; i++){
        markerArray[i].setMap(null);
    }
    hmc.clearMarkers();
    hmcWater.clearMarkers();
    hmcFood.clearMarkers();
    hmcElect.clearMarkers();
    hmcComun.clearMarkers();
    hmcPeople.clearMarkers();
    hmcSafety.clearMarkers();
    historicalLocations = new Array();
}

function historicalFilterChanged(){
     try{
      setHistoricalMarkers(currentHistoricalMarkerData);
    }
    catch(e){
      console.log("La información de los marcadores no está lista aun.");
    }
  }

var currentValueMin = -1;
var currentValueMax = -1;

/**
  Cada vez que cambia la fecha, se dispara.
*/
$("#histograma2").on('change', function(e, data){
   //console.log("Something moved. min: " + data.values.min + " max: " + data.values.max);
   //console.log(new Date(data.values.min).getTime());
   //console.log(new Date(data.values.max).getTime());
   removeHistoricalMarkers(historicalLocations);
   
   //currentValueMin = data.values.min;
   //currentValueMax = data.values.max;
   currentValueMin = valuesOfAxis[0];
   currentValueMax = valuesOfAxis[1];

   var json = (function () {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "getDateRangeLocations/"+new Date(currentValueMin).getTime()+"/"+new Date(currentValueMax).getTime()+"",
            'dataType': "json",
            'success': function (data) {
              //console.log(data);
              json = data;
            }
        });
        return json;
    })();
    currentHistoricalMarkerData = json;
    setHistoricalMarkers(json);
      
});

var hdelay = (function () {
  var hdelay = null;
  $.ajax({
    'async': false,
    'global': false,
    'url': "/config/getRefreshTime",
    'dataType': "json",
    'success': function (data) {
      hdelay = data;
    }
  });
  return hdelay*1000;
})(); 

setInterval(function () {
  if(currentValueMax != -1){
    var json = (function () {
    var json = null;
      $.ajax({
        'async': false,
        'global': false,
        'url': "getDateRangeLocations/"+new Date(currentValueMin).getTime()+"/"+new Date(currentValueMax).getTime()+"",
        'dataType': "json",
        'success': function (data) {
          json = data;
        }
      });
      return json;
    })(); 
    currentHistoricalMarkerData = json;
    //console.log(json);
    setHistoricalMarkers(currentHistoricalMarkerData);  
  }
  else{
    var json = (function () {
    var json = null;
      $.ajax({
        'async': false,
        'global': false,
        'url': "getLocations",
        'dataType': "json",
        'success': function (data) {
          json = data;
        }
      });
      return json;
    })(); 
    currentHistoricalMarkerData = json;
    //console.log(json);
    setHistoricalMarkers(currentHistoricalMarkerData);  
  }    
}, hdelay);


   
