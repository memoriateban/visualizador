function bindEvent(element, eventName, eventHandler) {
    var el = $(element)[0];
    if (el.addEventListener) {
        el.addEventListener(eventName, eventHandler, false)
    } else if (el.attachEvent) {
        el.attachEvent('on'+eventName, eventHandler);
    }
}
bindEvent('#b1', 'click', function() {
    $('#p1').toggle('blind');
    if ($('#b1').text() == 'Show Spoiler') {
        $('#b1').text('Hide Spoiler');
    } else if ($('#b1').text() == 'Hide Spoiler') {
        $('#b1').text('Show Spoiler');
    }

});

$(document).ready(function() { 
    
    $("span.spoiler").hide();
    
    $('<a class="reveal">Estadísticas</a> ').insertBefore('.spoiler');

    $("a.reveal").click(function(){
        $(this).parents("p").children("span.spoiler").fadeIn(1000);
        $(this).parents("p").children("a.reveal").fadeOut(1);
        $( "#statContainer" ).css("margin-left", "35%");
    });
}); 




