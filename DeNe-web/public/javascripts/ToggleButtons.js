 $(document).ready(function(){
        $("#statContainerButton").click(function(){
            $("#statContainer").fadeToggle( "slow", "linear" );
        });
    });

    $(document).ready(function(){
        $("#SimbologiaButton").click(function(){
            $("#Simbologia").fadeToggle( "slow", "linear" );
        });
    });

    $(document).ready(function(){
        $("#showUploadModel").click(function(){
            $("#uploadClassifier").fadeToggle( "slow", "linear" );
            //window.location.href = "#uploadClassifier";
            $('html, body').animate({
                scrollTop: $("#uploadClassifier").offset().top
            }, 1000);
        });
    });

    $(document).ready(function(){
        $("#hideUploadModel").click(function(){

            //window.location.href = "#configDeNe";
            $('html, body').animate({
                scrollTop: $("#configDeNe").offset().top
            }, 1000);
            $("#uploadClassifier").fadeToggle( "slow", "linear" );
            
        });
    });

    $(document).ready(function(){
        $('#botonFormulario').click(function(){
            $("#formularioTerminosBusqueda").fadeToggle( "slow", "linear" );
        });
    });

    $(document).ready(function(){
        $('#botonOpcionesAgrupamiento').click(function(){
            $("#formularioOpcionesAgrupamiento").fadeToggle( "slow", "linear" );
        });
    });

    $(document).ready(function(){
        $('#botonMostrarHistograma').click(function(){
            
            if(!$("#histogramaContenedorOculto").is(":visible")){
                $("#histogramaContenedorOculto").fadeToggle( "slow", "linear" );
                $('html, body').animate({
                    scrollTop: $("#botonMostrarHistograma").offset().top
                }, 1000);
                //location.href = "/#botonMostrarHistograma"
            }
            else{
                //location.href = "/#mapaHistorico"
                $('html, body').animate({
                    scrollTop: $("#mapaHistorico").offset().top
                }, 1000);
                $("#histogramaContenedorOculto").fadeToggle( "slow", "linear" );
            }
        });
    });

    $(document).ready(function(){
        $('#infoBotonHome').click(function(){
            $('#infoBotonHome').toggle()
            $("#appTittle").toggle()

            $("#infoHome").fadeToggle( "slow", "linear" );
            $("#infoHomeButtonClose").fadeToggle( "slow", "linear" );
            

        });
    });

    $(document).ready(function(){
        $('#infoHomeButtonClose').click(function(){
            
            $("#infoHome").toggle()
            $("#infoHomeButtonClose").toggle()

            $('#infoBotonHome').fadeToggle( "slow", "linear" );
            $("#appTittle").fadeToggle( "slow", "linear" );
        });
    });